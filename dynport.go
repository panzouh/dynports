package dynport

import (
	"errors"
	"log"
)

type PortNetGenerator struct {
	//state
	idx     int
	current int
}

func Increment(ports []int) {
	for i := len(ports) - 1; i >= 0; i-- {
		ports[i]++
		if ports[i] != 0 {
			break
		}
	}
}

func binarySearch(needle int, haystack []int) (int, error) {
	low := 0
	high := len(haystack) - 1

	for low <= high {
		median := (low + high) / 2

		if haystack[median] < needle {
			low = median + 1
		} else {
			high = median - 1
		}
	}

	if low == len(haystack) || haystack[low] != needle {
		err := errors.New("element not found in array")
		return 0, err
	}

	return low, nil
}

func NewFromPortArr(curPort int, portsArr []int) *PortNetGenerator {
	idx, err := binarySearch(curPort, portsArr)
	if err != nil {
		log.Fatalln(err)
	}
	newPort := portsArr[idx+1]

	return &PortNetGenerator{
		idx:     idx + 1,
		current: newPort,
	}
}

func DynRange(min, max int) []int {
	s := make([]int, max-min+1)
	for i := range s {
		s[i] = i + min
	}
	return s
}
